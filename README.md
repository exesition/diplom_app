# 3. Создание тестового приложения

## Подготовка:

Создаём структуру каталогов на локальной машине:<br>
`app`- основной каталог<br>
`conf` - подкаталог с настройками nginx<br>
`content`- подкаталог c статичными данными index.html<br>


В каталоге `app` создаём `Dockerfile`:
```text
FROM nginx:1.23.3

# Configuration 
ADD conf /etc/nginx
# Content
ADD content /usr/share/nginx/html

EXPOSE 80 
```

Cоздаём файл `~/app/conf/nginx.conf` с конфигурацией nginx:
```text
user nginx;
worker_processes 1;
error_log /var/log/nginx/error.log warn;

events {
    worker_connections 1024;
    multi_accept on;
}

http {
    server {
        listen   80;

        location / {
            gzip off;
            root /usr/share/nginx/html/;
            index index.html;
        }
    }
    keepalive_timeout  65;
}

```

Cоздаём статическую страницу нашего приложения `~/app/content/index.html`:
```text
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Netology DevOps Diplom App</title>
  <style>
    body {
      background-color: #f0f0f0;
      font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      min-height: 100vh;
      margin: 0;
    }

    .container {
      background-color: #fff;
      border-radius: 10px;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
      padding: 30px;
      text-align: center;
    }

    h1 {
      color: #333;
      font-size: 2.5em;
      margin-bottom: 20px;
    }

    p {
      color: #666;
      font-size: 1.2em;
      line-height: 1.6;
    }

    .logo {
      width: 150px;
      height: 150px;
      margin: 30px 0;
    }

    .logo img {
      width: 100%;
      height: 100%;
      object-fit: cover;
      border-radius: 50%;
    }

    .btn {
      background-color: #007bff;
      color: #fff;
      padding: 15px 30px;
      border: none;
      border-radius: 5px;
      font-size: 1.2em;
      cursor: pointer;
      transition: background-color 0.3s;
    }

    .btn:hover {
      background-color: #0056b3;
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="logo">
      <img
        src="https://avatars.mds.yandex.net/i?id=1be73cbd7693b6c0ae9ee5c97c2ceb168c71f5cb-10350562-images-thumbs&n=13"
        alt="Netology Logo">
    </div>
    <h1>Netology DevOps Diplom App</h1>
    <p>Добро пожаловать в приложение, разработанное в рамках дипломного проекта по DevOps в Нетологии.</p>
    <button class="btn">Подробнее</button>
  </div>
</body>

</html>
```

### Собираем docker-image:

Переходим в папку с `Dockerfile` и собираем образ:
```bash
docker build -t diplom_app .
```

Запускаем контейнер для проверки его работы:
```bash
docker run -d --name diplom_app -p80:80 diplom_app

docker ps -a
CONTAINER ID   IMAGE                                                 COMMAND                  CREATED         STATUS         PORTS                               NAMES
c2d2a3c688f4   diplom_app  "/docker-entrypoint.…"   4 seconds ago   Up 4 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp    diplom_app
```

Проверяем работу приложения:

- Http доступ к тестовому приложению:<br>
   - [84.201.152.15](http://84.201.152.15:30080)<br>
   - [62.84.116.11](http://62.84.116.11:30080)<br>
   - [158.160.158.194](http://158.160.158.194:30080)<br>

![Приложение работает](./img/03_01_apppage.png)

Приложение работает.

Останавливаем контейнер:
```bash
docker stop diplom_app
```

Подключаемся к DockerHub и загружаем образ в хранилище:
```bash
docker login -u exesition -p <password> exesition/diplom_app
docker push exesition/diplom_app:v1.0.0
```

Запуск из хранилища
```bash
docker run -d --name diplom_app -p80:80 exesition/diplom_app:v1.0.0
```

Ссылка на реестр с образом: https://hub.docker.com/repository/docker/exesition/diplom_app